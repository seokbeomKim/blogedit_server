/*
 * tasker queue
 *
 * backends 에서 client application으로 요청한 것에 대한 응답이 어떤 요청에 대한 것인지 파악하기 위해 tasker를 이용하여 전달받을 Response 값을 대기열에 추가한다.
 */
class Tasker {
  enqueue(item) {
    if (!this.items) {
      this.items = []
    }
    item.id = this.count()
    console.log('Tasker: new item(' + item.id + ') is added.')
    this.items.unshift(item)
  }
  dequeue() {
    if (this.items) {
      return this.items.pop()
    } else {
      return null
    }
  }
  /**
   * Queue에 등록된 아이템 중 caller handler에 맞는 task만을 선택한다.
   * @param  {String} operation 선택하고자 하는 task 종류
   */
  dequeue(operation) {
    let rvalue = null
    
    this.items.forEach((value, index, arr) => {
      if (value.operation === operation) {
        rvalue = value
        return
      }
    })
    
    return rvalue
  }
  count() {
    if (this.items) {
      return this.items.length
    }
    return 0
  }
}

var taskerObj = new Tasker()

module.exports = taskerObj
