class TaskerItem {
    constructor (operation, platform, auth, socket) {
        this.operation = operation;
        this.platform = platform;
        this.auth = auth;
        this.socket = socket;
    }
}

module.exports = TaskerItem;
