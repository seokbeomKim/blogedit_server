const {
  CommonAPI
} = require('./common')
const request = require('request')
const socket = require('blogedit/libs/sockets')
const tasker = require('../tasker')
const taskerItem = require('../tasker/taskerItem')
const Users = require('../schemas/blogAccount')
const checkEssentialQueries = require('blogedit/libs/checkQueries')

/**
 * wordpress.js
 *
 * 워드프레스 API 사용을 위한 Backends 인터페이스 정의 및 구현
 */
class WordpressAPI extends CommonAPI {
  constructor() {
    super('wordpress')
  }

  /**
   * RESTful API URLs
   * Administrator should install -
   * 'UWR(Ultimate Wordpress RESTful API)' Plugin
   */
  get urls() {
    return {
      login: '/wp-json/wp/v2/users/me/login',
      logout: '/wp-json/wp/v2/users/me/logout',
      validateToken: '/wp-json/wp/v2/users/me/token',
      posts: '/wp-json/wp/v2/posts',
      categories: '/wp-json/wp/v2/categories',
      comments: '/wp-json/wp/v2/comments',
      tags: '/wp-json/wp/v2/tags',
      media: '/wp-json/wp/v2/media'
    }
  }

  /**
   * 토큰 유효성 검사
   * 
   * @param  auth_info.url  해당 사용자의 워드프레스 사이트 주소
   * @param  req.body.token 검사하고자 하는 토큰
   * @param  {any} res 응답(Response) 객체
   */
  token_post(auth_info, req, res) {
    if (!auth_info.url) {
      throw new Error('워드프레스 계정의 URL 정보가 없습니다.')
    }
    let req_url = new URL(auth_info.url + this.urls.validateToken)

    request.post(req_url.href, {
      form: req.body
    }, function (err, response, body) {
      if (response.statusCode === 200) {
        res.send({
          success: 1,
          result: JSON.parse(body)
        })
      } else {
        res.send({
          success: 0,
          error: response.statusMessage
        })
      }
    })
  }

  /**
   * 현재 상태에서 유효한 access token을 가져온다.
   * 
   * @param  {any} auth_info 사용자 인증 정보
   * @param  {any} req 요청(Request) 객체
   * @param  {any} res 응답(Response) 객체
   */
  token_get(auth_info, req, res) {
    let req_url = new URL(auth_info.url + this.urls.login)
    request.post(req_url.href, {
      form: {
        "username": auth_info.username,
        "password": auth_info.password
      }
    }, function (err, response, body) {
      if (response.statusCode === 200) {
        res.send({
          success: 1,
          result: JSON.parse(body).data.token
        })
      } else {
        res.send({
          success: 0,
          error: response.statusMessage
        })
      }
    })
  }

  /**
   * 카테고리 한 개에 대한 정보를 가져온다.
   * 
   * @param  {} auth_info
   * @param  req.query.id 카테고리 id
   * @param  {} res
   */
  category_get(auth_info, req, res) {
    checkEssentialQueries(req.query, 'id')
      .then(() => {
        const req_url = auth_info.url + this.urls.categories + '/' + req.query.id

        request.get(req_url, (error, response, body) => {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            res.send({
              success: 0,
              error: response.statusMessage
            })
          }
        })
      })
      .catch(error => {
        res.send({
          success: 0,
          error: error
        })
      })
  }
  /**
   * 카테고리 한 개를 생성한다. 이 때, 반드시 카테고리명(req.body.name)과 JWT TOKEN(req.body.token)을 전달해야 한다.
   * 
   * @param  {any} auth_info
   * @param  req.body.description 카테고리 설명 
   * @param  req.body.token JWT token(required)
   * @param  req.body.name 카테고리명(required)
   * @param  req.body.slug 카테고리 slug
   * @param  req.body.parent 부모 카테고리 ID
   * @param  {Object} req.body.meta 카테고리 관련 메타 필드
   * @param  {any} res
   */
  category_post(auth_info, req, res) {
    checkEssentialQueries(req.body, 'name')
      .then(() => {
        const req_url = auth_info.url + this.urls.categories
        const headers = {
          'Authorization': 'Bearer ' + req.body.token,
          'Content-Type': 'application/json'
        }

        // 헤더 설정 후 삭제
        delete req.body.token

        const formData = req.body

        request.post({
          headers: headers,
          url: req_url,
          form: formData
        }, (error, response, body) => {
          if (response.statusCode === 201) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            res.send({
              success: 0,
              error: response.statusMessage
            })
          }
        })
      })
      .catch(error => {
        res.send({
          success: 0,
          error: error
        })
      })
  }

  /**
   * 전체 카테고리에 대한 정보를 가져온다.
   * @param  {} auth_info
   * @param  {} req
   * @param  {} res
   */
  categories_get(auth_info, req, res) {
    let req_url = new URL(auth_info.url + this.urls.categories)
    request.get(req_url.href, (error, response, body) => {
      if (response.statusCode === 200) {
        res.send({
          success: 1,
          result: JSON.parse(body)
        })
      } else {
        res.send({
          success: 0,
          error: response.statusMessage
        })
      }
    })
  }

  posts_get(auth_info, req, res) {
    let req_url = new URL(auth_info.url + this.urls.posts)
    request.get(req_url.href, (error, response, body) => {
      if (response.statusCode === 200) {
        res.send({
          success: 1,
          result: JSON.parse(body)
        })
      } else {
        res.send({
          success: 0,
          error: response.statusMessage
        })
      }
    })
  }

  post_get(auth_info, req, res) {
    checkEssentialQueries(req.query, 'id')
      .then(() => {
        const req_url = auth_info.url + this.urls.posts + '/' + req.query.id

        request.get(req_url, (error, response, body) => {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            res.send({
              success: 0,
              error: response.statusMessage
            })
          }
        })
      })
      .catch(error => {
        res.send({
          success: 0,
          error: error
        })
      })
  }

  post_post(auth_info, req, res) {
    checkEssentialQueries(req.body, 'title')
      .then(() => {
        const req_url = auth_info.url + this.urls.posts
        const headers = {
          'Authorization': 'Bearer ' + req.body.token,
          'Content-Type': 'application/json'
        }
        delete req.body.token
        const formData = req.body

        request.post({
          headers: headers,
          url: req_url,
          form: formData
        }, (error, response, body) => {
          if (response.statusCode === 201) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            res.send({
              success: 0,
              error: response.statusMessage
            })
          }
        })
      })
      .catch(error => {
        res.send({
          success: 0,
          error: error
        })
      })
  }

}

module.exports = WordpressAPI
