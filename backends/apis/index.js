const tistory_api = require('./tistory')
const wordpress_api = require('./wordpress')

module.exports = {
  tistory: new tistory_api(),
  wordpress: new wordpress_api(),
}
