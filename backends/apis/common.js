module.exports.CommonAPI = class CommonAPI {

  /**
   * 플랫폼 이름 설정과 함께 API 객체를 초기화한다.
   * @param {String} name 블로그 플랫폼 이름
   */
  constructor(name) {
    this.name = name
  }
}
