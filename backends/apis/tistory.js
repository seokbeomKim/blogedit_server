const {
  CommonAPI
} = require('./common')
const request = require('request')
const socket = require('blogedit/libs/sockets')
const tasker = require('../tasker')
const taskerItem = require('../tasker/taskerItem')
const Users = require('../schemas/blogAccount')
const checkEssentialQueries = require('blogedit/libs/checkQueries')

/**
 * TistoryAPI
 * 티스토리 API 사용을 위한 인터페이스 정의 및 구현 코드.
 * GET/POST/DELETE/PUT 등에 대한 핸들러는 접미사(_get/_post/_delete/_put)를 이용하여
 * 구현하며 클라이언트에서는 접미사를 제외한 나머지 부분만 이용하여 RESTful API를 이용하도록
 * 구현해야 한다. (예. GET /platforms/:blogAccountIndex/authentication_code)
 * 위 GET Request는 authentication_code_get 을 호출하게 구현되어 있다.
 */
class TistoryAPI extends CommonAPI {
  /**
   * tistory API 리소스 URL
   */
  get urls() {
    return {
      auth: 'https://www.tistory.com/oauth/authorize',
      access_token: 'https://www.tistory.com/oauth/access_token',
      blog_info: 'https://www.tistory.com/apis/blog/info',
      post_list: 'https://www.tistory.com/apis/post/list',
      post_read: 'https://www.tistory.com/apis/post/read',
      post_write: 'https://www.tistory.com/apis/post/write',
      post_modify: 'https://www.tistory.com/apis/post/modify',
      file_upload: 'https://www.tistory.com/apis/post/attach',
      category_list: 'https://www.tistory.com/apis/category/list',
      comments_list: 'https://www.tistory.com/apis/comment/list',
      comments_newest: 'https://www.tistory.com/apis/comment/newest',
      comment_write: 'https://www.tistory.com/apis/comment/write',
      comment_modify: 'https://www.tistory.com/apis/comment/modify',
      comment_delete: 'https://www.tistory.com/apis/comment/delete',
    }
  }

  constructor() {
    super('tistory')
  }

  /**
   * DB 내의 인증 정보를 이용하여 티스토리 API Access Code를
   * 얻어온다. 로컬 DB에 클라이언트에서 요청한 블로그 계정이 티스토리
   * API의 사용자 인증 정보를 사용할 것이므로 다른 블로그 엔진과의
   * 충돌 가능성은 고려하지 않아도 된다.
   *
   * @param  {Object} auth_info 로컬 DB로부터 받아온 사용자 인증 정보
   */
  authentication_code_get(auth_info, req, res) {
    if (!auth_info.client_id || !auth_info.callback_uri) {
      throw new Error('Could not find authentication info: ' + auth_info)
    }

    // 나중에 클라이언트에서 사용자가 직접 허용 후에 response를 저장해야 하므로 tasker queue에 // 해당 아이템을 저장한다.
    tasker.enqueue(new taskerItem('oauth', 'tistory', auth_info, socket))

    let req_url = new URL(this.urls.auth)
    let params = req_url.searchParams

    params.append('client_id', auth_info.client_id)
    params.append('redirect_uri', auth_info.callback_uri)
    params.append('response_type', 'code')

    try {
      socket.sendDataToClient(
        socket.SOCKET_REQUESTS.REQUEST_TISTORY_AUTH,
        req_url + params,
      )
    } catch (error) {
      throw new Error(error)
    }

    res.send({
      success: 1
    })
  }

  /**
   * 받아온 권한 증서를 이용하여 최종적으로 access code를 받아온다.
   * @param  {} authorization_code authorization Grant 코드
   * @param  {} auth_info 관련된 사용자 인증 정보
   */
  access_code_get(authorization_code, auth_info) {
    let req_url = new URL(this.urls.access_token)
    let params = req_url.searchParams
    params.append('client_id', auth_info.client_id)
    params.append('client_secret', auth_info.secret_key)
    params.append('redirect_uri', auth_info.callback_uri)
    params.append('code', authorization_code)
    params.append('grant_type', 'authorization_code')

    console.log(req_url.href)

    request.get(req_url.href, function (error, response, body) {
      if (error) {
        throw new Error('Unable to parse GET response')
      }

      const access_token = body.split('=')[1]
      if (!access_token) {
        throw new Error(
          'Succeed to GET response but there is no access code within the received data.',
        )
      }

      console.log('Set access code: ' + access_token)
      Users.updateOne({
          'auth.client_id': auth_info.client_id,
        }, {
          $set: {
            auth: {
              client_id: auth_info.client_id,
              secret_key: auth_info.secret_key,
              callback_uri: auth_info.callback_uri,
              access_token: access_token,
            },
          },
        },
        function (err, doc) {
          if (err) {
            throw new Error(err)
          }
        },
      )

      // 티스토리의 경우 access code를 받아온 뒤에는 창을 자동으로 닫아야 한다.
      try {
        socket.sendDataToClient(
          socket.SOCKET_REQUESTS.REQUEST_CLOSE_POPUP,
          'tistory_oauth',
        )
      } catch (error) {
        console.error(error)
        return error.message
      }

      return 'Request access code to the client.'
    })
  }

  /**
   * 사용자의 블로그 정보를 불러온다.
   * @param  {any} auth_info 사용자 인증 정보
   * @param {any} req blogInfo GET request 정보
   */
  blog_info_get(auth_info, req, res) {
    let req_url = new URL(this.urls.blog_info)
    let params = req_url.searchParams
    let rvalue

    params.append('access_token', auth_info.access_token)
    params.append('output', 'json')

    request.get(req_url.href, function (error, response, body) {
      if (response.statusCode === 200) {
        res.send({
          success: 1,
          result: JSON.parse(body)
        })
      } else {
        throw new Error(error)
      }
    })

    return rvalue
  }

  /**
   * 사용자 블로그에 저장되어 있는 포스팅 정보를 방아온다.
   * @param  {any} auth_info
   * @param  {any} req
   * @param  {any} res
   */
  posts_get(auth_info, req, res) {
    const blogName = req.query.blogName
    const pageNum = req.query.pageNum
    let req_url = new URL(this.urls.post_list)
    let params = req_url.searchParams

    params.append('access_token', auth_info.access_token)
    params.append('output', 'json')
    params.append('blogName', blogName)

    if (pageNum) {
      params.append('page', pageNum)
    }

    request.get(req_url.href, (error, response, body) => {
      if (response.statusCode === 200) {
        res.send({
          success: 1,
          result: JSON.parse(body)
        })
      } else {
        throw new Error(error)
      }
    })
  }

  /**
   * 글 읽기 API (포스팅 한 개의 정보를 가져온다.)
   * @param  {} auth_info 사용자 인증 정보
   * @param  {} req request 객체
   * @param  {} res response 객체
   */
  post_get(auth_info, req, res) {
    let req_url = new URL(this.urls.post_read)
    let params = req_url.searchParams

    checkEssentialQueries(req.query, 'blogName', 'postId')
      .then(() => {
        params.append('access_token', auth_info.access_token)
        params.append('output', 'json')
        params.append('blogName', req.query.blogName)
        params.append('postId', req.query.postId)

        request.get(req_url.href, (error, response, body) => {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            res.send({
              success: 0,
              error: JSON.parse(error)
            })
          }
        })
      })
      .catch((error) => {
        throw new Error(error)
      })
  }

  /**
   * 티스토리 블로그에 글 포스팅 (post_write)
   * 
   * @param  {any} auth_info 사용자 인증 정보
   * @param  {any} req 요청(request) 관리 객체
   * @param  {any} res 응답(response) 관리 객체
   */
  post_post(auth_info, req, res) {
    let req_url = new URL(this.urls.post_write)

    checkEssentialQueries(req.body, 'blogName', 'title')
      .then(() => {
        // 기본 매개변수 설정
        req.body.access_token = auth_info.access_token
        req.body.output = 'json'

        request.post(req_url.href, {
          form: req.body
        }, function (error, response, body) {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            throw new Error(error)
          }
        })
      })
      .catch((error) => {
        throw new Error(error)
      })
  }

  /**
   * 티스토리 블로그에 글 수정 (post_modify)
   * @param  {any} auth_info 사용자 인증 정보
   * @param  {any} req 요청(request) 관리 객체
   * @param  {any} res 응답(response) 관리 객체
   */
  post_put(auth_info, req, res) {
    let req_url = new URL(this.urls.post_modify)

    checkEssentialQueries(req.body, 'blogName', 'postId', 'title')
      .then(() => {
        // 기본 매개변수 설정
        req.body.access_token = auth_info.access_token
        req.body.output = 'json'

        request.post(req_url.href, {
          form: req.body
        }, function (error, response, body) {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            res.send({
              succes: 0,
              error: JSON.parse(body).tistory.error_message
            })
          }
        })
      })
      .catch((error) => {
        throw new Error(error.message)
      })
  }

  /**
   * 티스토리에 파일 첨부(file_upload)
   * API 호출을 위해 반드시 첨부하고자 하는 파일의 파일경로를 전달받아야 한다.
   * 
   * @param  {any} auth_info 사용자 인증 정보
   * @param  {any} req 요청(request) 관리 객체
   * @param  {any} res 응답(response) 관리 객체
   */
  upload_post(auth_info, req, res) {
    let req_url = this.urls.file_upload

    checkEssentialQueries(req.body, 'blogName', 'uploadedfile')
      .then(() => {
        req.body.access_token = auth_info.access_token
        request.post(req_url.href, {
          form: req.body
        }, function (error, response, body) {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            res.send({
              succes: 0,
              error: JSON.parse(body).tistory.error_message
            })
          }
        })
      })
      .catch(error => {
        if (typeof error === 'string') {
          res.send({
            success: 0,
            error: error
          })
        } else {
          res.send({
            success: 0,
            error: error.message
          })
        }
      })
  }

  /**
   * 티스토리의 카테고리 목록을 가져온다
   * 
   * @param  {Object} auth_info 사용자 인증 정보
   * @param  {Object} req 요청(request) 관리 객체
   * @param  {Object} res 응답(response) 관리 객체
   */
  categories_get(auth_info, req, res) {
    let req_url = new URL(this.urls.category_list)
    let params = req_url.searchParams

    checkEssentialQueries(req.query, 'blogName')
      .then(() => {
        params.append('access_token', auth_info.access_token)
        params.append('output', 'json')
        params.append('blogName', req.query.blogName)

        request.get(req_url.href, (error, response, body) => {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            res.send({
              success: 0,
              error: JSON.parse(error)
            })
          }
        })
      })
      .catch((error) => {
        throw new Error(error)
      })
  }


  /**
   * 코멘트 목록을 가져온다.
   * 
   * @param  {Object} auth_info 사용자 인증 정보
   * @param  {Object} req 요청(request) 관리 객체
   * @param  {Object} res 응답(response) 관리 객체
   */
  comments_get(auth_info, req, res) {
    let req_url = new URL(this.urls.comments_list)
    let params = req_url.searchParams

    checkEssentialQueries(req.query, 'blogName', 'postId')
      .then(() => {
        params.append('access_token', auth_info.access_token)
        params.append('output', 'json')
        params.append('blogName', req.query.blogName)
        params.append('postId', req.query.postId)

        request.get(req_url.href, (error, response, body) => {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            res.send({
              success: 0,
              error: JSON.parse(error)
            })
          }
        })
      })
      .catch((error) => {
        throw new Error(error)
      })
  }
  /**
   * 티스토리 블로그에 댓글 작성
   * 
   * @param  {any} auth_info
   * @param req.blogName  블로그 이름(필수)
   * @param req.postId    댓글 추가 대상 포스팅 ID(필수)
   * @param req.parentId  부모 댓글 ID (대댓글인 경우에 사용)
   * @param req.content   댓글 내용
   * @param req.secret    비밀 댓글 여부 (1: 비밀 댓글, 0: 공개 댓글-기본)
   * @param  {any} res
   */
  comment_post(auth_info, req, res) {
    let req_url = new URL(this.urls.comment_write)

    checkEssentialQueries(req.body, 'blogName', 'postId')
      .then(() => {
        req.body.access_token = auth_info.access_token
        req.body.output = 'json'

        request.post(req_url.href, {
          form: req.body
        }, function (err, response, body) {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            throw new Error(error)
          }
        })
      })
      .catch(error => {
        throw new Error(error)
      })
  }

  /**
   * 티스토리 블로그에 댓글 수정
   * 
   * @param  {any} auth_info
   * @param req.blogName  블로그 이름(필수)
   * @param req.postId    댓글 추가 대상 포스팅 ID(필수)
   * @param req.commentId 댓글 ID (필수)
   * @param req.parentId  부모 댓글 ID (대댓글인 경우 허용)
   * @param req.content   댓글 내용
   * @param req.secret    비밀 댓글 여부 (1: 비밀 댓글, 0: 공개 댓글-기본)
   * @param  {any} res
   */
  comment_put(auth_info, req, res) {
    let req_url = new URL(this.urls.comment_modify)

    checkEssentialQueries(req.body, 'blogName', 'postId', 'commentId')
      .then(() => {
        req.body.access_token = auth_info.access_token
        req.body.output = 'json'

        request.post(req_url.href, {
          form: req.body
        }, function (err, response, body) {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            throw new Error(error)
          }
        })
      })
      .catch(error => {
        throw new Error(error)
      })
  }

  /**
   * 티스토리 블로그에 댓글 수정
   * 
   * @param  {any} auth_info
   * @param req.blogName  블로그 이름(필수)
   * @param req.postId    댓글 추가 대상 포스팅 ID(필수)
   * @param req.commentId 댓글 ID (필수)
   * @param  {any} res
   */
  comment_delete(auth_info, req, res) {
    let req_url = new URL(this.urls.comment_delete)

    checkEssentialQueries(req.body, 'blogName', 'postId', 'commentId')
      .then(() => {
        req.body.access_token = auth_info.access_token
        req.body.output = 'json'

        request.post(req_url.href, {
          form: req.body
        }, function (err, response, body) {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            throw new Error(error)
          }
        })
      })
      .catch(error => {
        throw new Error(error)
      })
  }

  /**
   * 최근 댓글 목록을 가져온다.
   * 
   * @param  {Object} auth_info 사용자 인증 정보
   * @param  {Object} req 요청(request) 관리 객체
   * @param  {Object} res 응답(response) 관리 객체
   */
  recent_comments_get(auth_info, req, res) {
    let req_url = new URL(this.urls.comments_newest)
    let params = req_url.searchParams

    checkEssentialQueries(req.query, 'blogName')
      .then(() => {
        params.append('access_token', auth_info.access_token)
        params.append('output', 'json')
        params.append('blogName', req.query.blogName)
        if (req.query.page)
          params.append('page', req.query.page)
        if (req.query.count)
          params.append('count', req.query.count)

        request.get(req_url.href, (error, response, body) => {
          if (response.statusCode === 200) {
            res.send({
              success: 1,
              result: JSON.parse(body)
            })
          } else {
            res.send({
              success: 0,
              error: JSON.parse(error)
            })
          }
        })
      })
      .catch((error) => {
        throw new Error(error.message)
      })
  }


}

module.exports = TistoryAPI
