const express = require('express')
const router  = express.Router()

const category_controller = require('../controllers/categoryController')

router.get    ('/', category_controller.category_index_get)
router.put    ('/', category_controller.category_index_put)
router.post   ('/', category_controller.category_index_post)
router.delete ('/', category_controller.category_index_delete)

module.exports = router
