const express = require('express')
const router = express.Router()
const controller = require('../controllers/oauthController')

router.get('/:platform', controller.oauth_get)
router.post('/:platform', controller.oauth_post)

module.exports = router
