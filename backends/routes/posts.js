const express = require('express')
const router = express.Router()

const controller = require('../controllers/postController')

/**
 * Definitions of routers related to posts
 */
router.get    ('/', controller.posts_get)
router.post   ('/', controller.posts_post)
router.put    ('/', controller.posts_put)
router.delete ('/', controller.posts_delete)

module.exports = router
