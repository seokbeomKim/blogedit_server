const express = require('express')
const router = express.Router()

const controller = require('../controllers/categoryGroupController')

router.get    ('/', controller.category_grp_get)
router.post   ('/', controller.category_grp_post)
router.put    ('/', controller.category_grp_put)
router.delete ('/', controller.category_grp_delete)

module.exports = router
