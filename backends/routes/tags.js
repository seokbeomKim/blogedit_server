const tags_controller = require('../controllers/tagsController')
const express  = require('express')
const router   = express.Router()

/**
 * /tags routers
 * - GET    : 현재 DB에 저장되어 있는 태그 리스트 반환
 * - PUT    : _id값 또는 이름 이용하여 전달한 이름, 설명으로 업데이트
 * - POST   : POST 값으로 전달 받은 태그 이름으로 태그 생성
 * - DELETE : DELETE 메서드로 전달 받은 _id 혹은 태그명에 해당하는 태그 삭제
 */
router.get    ('/', tags_controller.tags_index_get)
router.post   ('/', tags_controller.tags_index_post)
router.delete ('/', tags_controller.tags_index_delete)
router.put    ('/', tags_controller.tags_index_put)

module.exports = router
