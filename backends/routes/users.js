var express = require('express');
var router = express.Router();

const controller = require('../controllers/userController')

router.get    ('/', controller.users_get)
router.put    ('/', controller.users_put)
router.post   ('/', controller.users_post)
router.delete ('/', controller.users_delete)

module.exports = router;
