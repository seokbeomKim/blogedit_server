var express = require('express')
var router = express.Router()

const controller = require('../controllers/blogEngineController')

/**
 * /platforms
 *
 * 로컬 데이터베이스 관리 API
 */
router.get('/', controller.platform_index_get)
router.put('/', controller.platform_index_put)
router.post('/', controller.platform_index_post)
router.delete('/', controller.platform_index_delete)

/**
 * /platforms/:account_id/:operations
 *
 * 블로그 엔진에 관련된 API 이용을 위한 리소스 및 라우터 정의
 */
router.get('/:account_id/:api_call', controller.platform_api_call)
router.put('/:account_id/:api_call', controller.platform_api_call)
router.post('/:account_id/:api_call', controller.platform_api_call)
router.delete('/:account_id/:api_call', controller.platform_api_call)

module.exports = router
