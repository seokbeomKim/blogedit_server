const Posts = require('../schemas/post')
const {getMessageWithCode: ErrorCode} = require('./errors.js')

/**
 * posts_get()
 *
 * 로컬 db에 있는 포스팅 반환
 *
 * GET       /posts
 * PARAMS    (OPTIONAL) tags ([])
 *           (OPTIONAL) categories ([])
 *           (OPTIONAL) category_groups ([])
 *           (OPTIONAL) title
 *           (OPTIONAL) id
 * RETURNS
 * 파라미터가 주어지지 않을 경우, {_id, title, categories, tags} 조합으로 반환
 */
exports.posts_get = async function (req, res) {
  const id              = req.query.id
  const title           = req.query.title
  const tags            = req.query.tags
  const categories      = req.query.categories
  const category_groups = req.query.category_groups

  if (!id && !title && !tags && !categories && !category_groups) {
    const posts = await Posts.find(req.query)
    res.status(200).json({posts: posts})
  } else {
    // 파라미터 중 한 개라도 명시되어 요청이 들어온 경우
    const posts = await Posts.find(
            {$or:
             [
               {_id: id},
               {title: title},
               {tags: {$in: tags}}, // 이 때 tags는 반드시 배열 형태여야 한다.
               {categories: {$in: categories}},
               {category_group: {$in: category_groups}}
             ]},
            {_id:1, title:1, categories:1, tags:1})
  }
}

/**
 * posts_post()
 *
 * 새 포스팅을 등록한다. 이 때, 연동할 블로그 플랫폼(카테고리 통해
 * 판단)이 주어지지 않은 경우에는 단순히 로컬 DB에만 포스팅을
 * 등록한다. 연동 대상이 주어진 경우에는 각 플랫폼 별 핸들러를
 * 호출하여 처리를 위임하여 후처리를 진행하고 전처리로서 우선적으로
 * Response를 전송한다.
 *
 * POST     /posts
 * PARAMS   (REQUIRED) title, content
 *          (OPTIONAL) tags, categories, category_groups
 * RETURNS
 * - 성공:  {success: 1, posts}
 * - 실패:  {success: 0, error}
 */
exports.posts_post = async function (req, res) {
  const title = req.body.title
  const content = req.body.content
  const tags = req.body.tags
  const categories = req.body.categories
  const category_groups = req.body.category_groups

  if (!title && !content) {
    res.send({
      success: 0,
      error: {
        code: 701,
        detail: ErrorCode(701)
      }
    })
  } else {
    const record = new Posts({
      title: title,
      content: content,
      tag: tags,
      category: categories,
      category_group: category_groups
    })
    await record
      .save()
      .then(result => {
        res.send({
          success: 1,
          posts: result
        })
      })
      .catch(error => {
        res.send({
          success: 0,
          error: {
            code: 706,
            detail: error
          }
        })
      })
  }
}

/**
 * posts_put()
 *
 * 기존 포스팅을 업데이트 한다. post()와 마찬가지로 연동할 블로그
 * 플랫폼이 주어질 경우 해당 플랫폼 핸들러를 통해 처리하고 그 외에는
 * 모두 로컬 핸들러 범위로 판단하여 처리한다.
 *
 * PUT      /posts
 * PARAMS   (REQUIRED) id
 *          (OPTIONAL) updateTo: {title, content, tags, categories, category_groups}
 *
 * RETURNS
 * - 성공: {success: 1, updated}
 * - 실패: {success: 0, error}
 */
exports.posts_put = async function (req, res) {
  const id       = req.body.id
  const updateTo = req.body.updateTo

  if (!id) {                    // id가 없는 경우, 업데이트 불가능
    res.send({success: 0, error: {code: 701, detail: ErrorCode(701)}})
  } else {
    await Posts
      .updateOne({_id: id}, {$set: updateTo}, function (err, updated) {
        if (err) {
          res.send({success: 0, updated: [updated],
                    error: {
                      code: 705,
                      detail: err
                    }})
        } else {
          res.send({success: 1, updated: updated})
        }
      })
  }
}

/**
 * posts_delete()
 *
 * 포스팅 삭제. 이 때 삭제 대상의 포스팅에 설정되어 있는 카테고리를
 * 항목을 참고하여 외부 블로그 플랫폼의 포스팅도 삭제 요청을 전송한다.
 *
 * DELETE   /posts
 * PARAMS   (REQUIRED) id
 *
 * RETURNS
 * - 성공: {success: 1, removed}
 * - 실패: {success: 0, error}
 */
exports.posts_delete = async function (req, res) {
  const post_id = req.body.id

  if (!post_id) {
    res.send({success: 0, error: {code: 701, detail: ErrorCode(701)}})
  } else {
    await Posts.deleteOne({_id: post_id}, function (err) {
      if (err) {
        res.send({success: 0, target: info, error: {code: 704, detail: err}})
      } else {
        res.send({success: 1, target: info})
      }
    })
  }
}
