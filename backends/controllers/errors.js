/**
 * errors.js
 *
 * blogedit 전용으로 response로 보낼 에러 정의
 * HTTP status code 와 헷갈리지 않도록 700번부터 시작한다.
 */
const codes = {
        "701": "Wrong request format. Please check your parameters.",
        "702": "The tag is already inserted.",
        "703": "The target tag does not exists.",
        "704": "Failed to remove the target.",
        "705": "Failed to update the target.",
        "706": "Failed to insert the target.",
        "707": "Failed to get items.",
        "708": "Undefined API: the requested API method was not found."
      }

const getMessageWithCode = (num) => { return codes[num] }

module.exports = {
  codes,
  getMessageWithCode
}
