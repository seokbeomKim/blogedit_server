const CategoryGroup = require('../schemas/categoryGroup')
const {getMessageWithCode:ErrorCode} = require('./errors.js')

/**
 * category_grp_get()
 * 데이터베이스에 저장되어 있는 카테고리 그룹 리스트를 반환한다.
 *
 * GET    /category_groups
 * PARAMS (OPTIONAL) name: 카테고리 그룹 이름 (명시하지 않을 경우 전체 반환)
 * RETURN category_groups
 *        [{_id, name, description, categories:{_id,name,description}}]
 */
exports.category_grp_get = function (req, res) {
  const req_name = req.query.name

  if (req_name) {
    CategoryGroup
      .find({name: req_name}, {_id:1, name:1, description:1, categories:1},
            function (err, data) {
              res.send({categories: data})
            })
  } else {
    CategoryGroup
      .find({}, {_id:1, name:1, description:1, categories:1},
            function (err, data) {
              res.send({categories: data})
            })
  }
}

/**
 * category_grp_post()
 * 새로운 카테고리 그룹 등록
 *
 * POST    /category_groups
 * PARAMS  (REQUIRED) name: 카테고리 그룹 이름
 *         (REQUIRED) desc: 카테고리 그룹 설명
 *         (OPTIONAL) categories: 그룹에 포함시킬 카테고리 _id 값 배열
 * RETURN  등록된 카테고리 그룹 정보
 */
exports.category_grp_post = async function (req, res) {
  const name = req.body["name"]
  const desc = req.body["desc"]
  const categories = req.body["categories"]

  if (name && desc) {
    const record = new CategoryGroup({
            name: name,
            description: desc,
            categories: categories
          })
    await record
      .save()
      .then(result => {
        res.send({success: 1, categories: result})
      })
      .catch(error => {
        res.send({success: 0, categories: [], error: {code: 706, detail: error}})
      })
  } else {
    res.send({error: {code: 701, detail: ErrorCode(701)}})
  }
}

/**
 * category_grp_put()
 * 카테고리 그룹 업데이트
 *
 * PUT    /category_groups
 * PARAMS (REQUIRED) id 또는 name 값
          (REQUIRED) updateTo {name, description, categories}: 업데이트할 내용
 * RETURN {success, target}
 */
exports.category_grp_put = async function (req, res) {
  const req_id = req.body["id"]
  const req_name = req.body["name"]
  const req_updateTo = req.body["updateTo"]

  if (!req_id && !req_name) {
    res.send({success: 0, error: {code: 701, detail: ErrorCode(701)}})
  } else {
    const info = await CategoryGroup.findOne({$or: [{_id: req_id}, {name: req_name}]},
                                       {_id:1, name:1, description:1, categories:1})
    if (info) {
      await CategoryGroup
        .updateOne({$or: [{_id: req_id}, {name: req_name}]},
                {$set: req_updateTo},
                function (err, updated) {
                  if (err) {
                    res.send({success: 0, updated: [updated],
                              error: {
                                code: 705,
                                detail: err
                              }})
                  } else {
                    res.send({success: 1, updated: updated})
                  }
                })
    } else {
      res.send({success: 0, error: {code: 705, detail: ErrorCode(705)}})
    }
  }
}

/**
 * category_grp_delete()
 * 카테고리 그룹 삭제
 *
 * DELETE    /category_groups
 * PARAMS    (REQUIRED) id 또는 name 중 한 가지는 반드시 주어져야 함
 *           (REQUIRED) updateTo {name, desc, categories:[{_id}]}
 * RETURNS
 * - 성공: {success; 1, category_group: {_id, name, description, categories:[]}
 * - 실패: {success: 0, category_group: [], error: {code, detail}}
 */
exports.category_grp_delete = async function (req, res) {
  const req_id = req.body["id"]
  const req_name = req.body["name"]

  if (!req_id && !req_name) {
    res.send({success: 0, category_group: [],
              error: {code: 701, detail: ErrorCode(701)}})
  } else {
    const info = await CategoryGroup
          .findOne({$or: [{_id: req_id}, {name: req_name}]},
                   {_id:1, name:1, description:1, categories:1})

    if (info) {
      CategoryGroup.deleteOne({$or: [{_id: req_id}, {name: req_name}]}, function (err) {
        if (err) {
          res.send({success: 0, target: info, error: {code: 704, detail: err}})
        } else {
          res.send({success: 1, target: info})
        }
      })
    } else {
      res.send({success:0, error:{code: 704, detail: ErrorCode(704)}})
    }
  }
}
