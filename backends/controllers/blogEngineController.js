const Platform = require('../schemas/blogEngine')
const Accounts = require('../schemas/blogAccount')
const mongoose = require('mongoose')
const {
  Types: {
    ObjectId
  }
} = mongoose
const {
  getMessageWithCode: ErrorCode
} = require('./errors')

const apis = require('../apis')

/**
 * platform_index_get()
 *
 * 데이터베이스에 저장되어 있는 블로그 엔진 리스트를 반환한다.
 *
 * GET     /platforms
 * PARAMS  (OPTIONAL) name: 플랫폼 이름
 * RETURNS platforms: [{_id, name, url}]
 */
exports.platform_index_get = function (req, res) {
  const req_name = req.query.name
  if (req_name) {
    Platform
      .find({
          name: req_name
        }, {
          _id: 1,
          name: 1,
          url: 1
        },
        function (err, data) {
          res.send({
            platforms: data
          })
        })
  } else {
    Platform
      .find({}, {
          _id: 1,
          name: 1,
          url: 1
        },
        function (err, data) {
          res.send({
            platforms: data
          })
        })
  }
}

/**
 * platform_index_put()
 *
 * DB에 저장된 플랫폼 정보를 변경한다.
 *
 * PUT     /platforms
 * PARAMS  (REQUIRED) name 또는 id
 *         (REQUIRED) updateTo {name, url}
 */
exports.platform_index_put = async function (req, res) {
  const req_id = req.body["id"]
  const req_name = req.body["name"]
  const req_updateTo = req.body["updateTo"]

  if (!req_id && !req_name) {
    res.send({
      success: 0,
      error: {
        code: 701,
        detail: ErrorCode(701)
      }
    })
  } else {
    const info = await Platform.findOne({
      $or: [{
        _id: req_id
      }, {
        name: req_name
      }]
    }, {
      _id: 1,
      name: 1,
      url: 1
    })
    if (info) {
      await Platform
        .updateOne({
            $or: [{
              _id: req_id
            }, {
              name: req_name
            }]
          }, {
            $set: req_updateTo
          },
          function (err, updated) {
            if (err) {
              res.send({
                success: 0,
                updated: [updated],
                error: {
                  code: 705,
                  detail: err
                }
              })
            } else {
              res.send({
                success: 1,
                updated: [updated]
              })
            }
          })
    } else {
      res.send({
        success: 0,
        error: {
          code: 705,
          detail: ErrorCode(705)
        }
      })
    }
  }
}

/**
 * platform_index_post()
 *
 * DB에 새로운 플랫폼 정보를 추가한다.
 *
 * POST    /platforms
 * PARAMS  (REQUIRED) name: 플랫폼 이름
 *         (REQUIRED) url: 플랫폼 관련 URI
 */
exports.platform_index_post = async function (req, res) {
  const req_name = req.body["name"]
  const req_url = req.body["url"]

  if (req_name && req_url) {
    const record = new Platform({
      name: req_name,
      url: req_url
    })
    await record
      .save()
      .then(result => {
        res.send({
          success: 1,
          platforms: result
        })
      })
      .catch(error => {
        res.send({
          success: 0,
          platforms: [],
          error: {
            code: 706,
            detail: error.message
          }
        })
      })
  } else {
    res.send({
      success: 0,
      platforms: [],
      error: {
        code: 706,
        detail: ErrorCode(706)
      }
    })
  }
}

/**
 * platform_index_delete()
 *
 * 플랫폼 정보 삭제
 *
 * DELETE    /platforms
 * PARAMS    (REQUIRED) id: 플랫폼 DB 내 _id
 */
exports.platform_index_delete = async function (req, res) {
  const req_id = req.body["id"]
  const info = await Platform.findOne({
    _id: req_id
  }, {
    _id: 1,
    name: 1,
    url: 1
  })

  if (info) {
    Platform.deleteOne({
      _id: req_id
    }, function (err) {
      if (err) {
        res.send({
          success: 0,
          removed: info,
          error: {
            code: 704,
            detail: err
          }
        })
      } else {
        res.send({
          success: 1,
          removed: info
        })
      }
    })
  } else {
    res.send({
      success: 0,
      error: {
        code: 704,
        detail: ErrorCode(704)
      }
    })
  }
}

/**
 * 각 블로그 엔진 별 API 호출 시에 공통적으로 사용되는 부분 정리
 * 
 * @async
 * @param  {ObjectId} account_id 로컬 DB 내 계정 index
 * @returns {platform_auth, platform_name} 사용자 계정에 관련된 사용자 인증 정보와 블로그 플랫폼명 반환
 */
const getPlatformInformation = async function (account_id) {
  let platform_id, platform_name, platform_auth

  try {
    const accountInfo = await Accounts.findOne({
      _id: ObjectId(account_id)
    }, {
      _id: 0,
      blogEngine: 1,
      auth: 1
    })
    platform_id = accountInfo['blogEngine']
    platform_auth = accountInfo['auth']
  } catch (error) {
    throw new Error(error.message)
  }

  if (platform_id) {
    try {
      platform_name = await Platform.findOne({
        _id: platform_id
      }, {
        _id: 0,
        name: 1
      })
    } catch (error) {
      throw new Error(error.message)
    }
  } else {
    throw new Error('Platform id(' + platform_id + ') is not available.')
  }

  platform_name = platform_name.name

  return {
    platform_auth,
    platform_name
  }
}

/**
 * platform_api_call_get()
 *
 * GET     /platforms/:blogaccount_id/:api_method
 *
 * PARAMS  blogaccount_id: 호출하고자 하는 API에 관련된 블로그 계정 id
 *         api_method: 해당 계정에서 호출하고자 하는 API Method
 */
exports.platform_api_call = async function (req, res) {
  const account_id = req.params.account_id
  const api_call = req.params.api_call + '_' + req.method.toLowerCase()

  let {
    platform_auth,
    platform_name
  } = await getPlatformInformation(account_id).catch(error => {
    res.send({
      success: 0,
      error: error.message
    })
  })

  // 요청한 API가 정의되어 있는 API인지 확인한다.
  var api = apis[platform_name]
  if (api_call in api && typeof api[api_call] === 'function') {
    try {
      api[api_call](platform_auth, req, res)
    } catch (error) {
      res.send({
        success: 0,
        error: error.message
      })
    }
  } else {
    res.send({
      success: 0,
      error: {
        code: 708,
        error: ErrorCode(708)
      }
    })
  }
}
