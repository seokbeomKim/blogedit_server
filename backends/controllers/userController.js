const Users = require('../schemas/blogAccount')
const {getMessageWithCode: ErrorCode} = require('./errors.js')
const Mongoose = require('mongoose')
const ObjectId = Mongoose.Types.ObjectId

/**
 * users_get()
 *
 * DB에 등록된 사용자 리스트 반환
 *
 * GET     /users
 * RETURNS
 * - 성공: {users: []}
 * - 실패: {users: [], error: {code, detail}}
 */
 exports.users_get = async function (req, res) {
  const users = await Users.find(req.query, function (err, data) {
    if (err) {
      res.send({users:[], error:{code: 707, detail: ErrorCode(707)}})
    } else {
      res.send({users:data})
    }
  })
}

/**
 * users_post()
 *
 * 사용자 추가 등록
 *
 * POST    /users
 * PARAMS  (REQUIRED) account: 계정 아이디
 *         (REQUIRED) platform_id: DB 내 블로그 엔진 _id 값
 *         (OPTIONAL) auth: 계정 관련 인증 정보
 * RETURNS
 * - 성공: {success: 1, users}
 * - 실패: {success: 0, error {code, detail}}
 */
exports.users_post = async function (req, res) {
  console.log('POST: /users')
  
  const account     = req.body.account
  const platform_id = req.body.platform_id
  const blogName    = req.body.blogName
  const blogUrl     = req.body.blogUrl

  if (!account && !platform_id) {
    res.send({success: 0, error: {code: 701, detail: ErrorCode(701)}})
  } else {
    const record = new Users({
            'account': account,
            'blogEngine': ObjectId(platform_id),
            'auth': {
              'username': req.body['auth[username]'],
              'password': req.body['auth[password]'],
              'token': req.body['auth[token]'],
              'url': req.body['auth[url]'],
              'client_id': req.body['auth[client_id]'],
              'secret_key': req.body['auth[secret_key]'],
              'callback_uri': req.body['auth[callback_uri]'],
              'access_token': req.body['auth[access_token]']
            },
            'blogName': blogName,
            'blogUrl': blogUrl,
            'blogNickname': req.body.blogNickname,
            'comment': req.body.comment
          })
    await record
      .save()
      .then(result => {
        res.send({success: 1, categories: result})
      })
      .catch(error => {
        res.send({success: 0, categories: [], error: {code: 706, detail: error}})
      })
  }
}

/**
 * users_put()
 *
 * 기존 사용자 정보 수정
 *
 * PUT     /users
 * PARAMS  (REQUIRED) id: DB 내의 사용자 _id 값
 *         (REQUIRED) updateTo {account, platform_id, auth}
 * RETURNS
 * - 성공: {success: 1, updated}
 * - 실패: {success: 0, error}
 */
exports.users_put = async function (req, res) {
  const req_id = req.body.id
  const updateTo = JSON.parse(req.body.updateTo)
  updateTo.blogEngine = ObjectId(updateTo.platform_id)

  console.log('req_id', req_id)
  console.log('updateTo:', updateTo)
  if (!req_id && !updateTo) {
    res.send({success: 0, error: {code: 701, detail: ErrorCode(701)}})
  } else {
    await Users
      .updateOne({_id: req_id}, {$set: updateTo}, function (err, updated) {
        if (err) {
          res.send({success: 0, updated: [updated],
                    error: {
                      code: 705,
                      detail: err
                    }})
        } else {
          res.send({success: 1, updated: updated})
        }
      })
  }
}

/**
 * users_delete()
 * 사용자 삭제
 *
 * DELETE   /users
 * PARAMS   (REQUIRED)id: 삭제할 계정의 _id값
 * RETURNS
 * - 성공: {success: 1, removed: {}}
 * - 실패: {success: 0, error: {code, detail}}
 */
exports.users_delete = async function (req, res) {
  const req_id = req.body.id

  if (!req_id) {
    res.send({success: 0,
              error: {code: 701, detail: ErrorCode(701)}})
  } else {
    const info = await Users
          .findOne({_id: req_id},
                   {_id:1, account:1, blogEngine:1, auth:1})

    if (info) {
      await Users.deleteOne({_id: req_id}, function (err) {
        if (err) {
          res.send({success: 0, target: info, error: {code: 704, detail: err}})
        } else {
          res.send({success: 1, target: info})
        }
      })
    } else {
      res.send({success:0, error:{code: 704, detail: ErrorCode(704)}})
    }
  }
}
