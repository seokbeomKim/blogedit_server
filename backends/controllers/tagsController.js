const Tags = require('../schemas/tag.js')
const {getMessageWithCode: ErrorCode} = require('./errors.js')

/**
 * tags_index_get()
 * 현재 DB에 저장되어 있는 태그 리스트 반환
 *
 * GET     /tags
 * PARAMS  (OPTIONAL) name
 *         태그 이름(명시하지 않을 경우 전체 반환)
 * RETURN  tags:[{_id, name, description}]
 */
exports.tags_index_get = function (req, res) {
  const req_name = req.query.name

  if (req_name) {
    Tags.find({name: req_name}, {_id:1, name:1, description:1},
              function (err, data) {
                res.send({tags: data})
              })
  } else {
    Tags.find({}, {_id:1, name:1, description:1},
              function (err, data) {
                res.send({tags: data})
              })
  }
}

/**
 * tags_index_post()
 * POST 값으로 전달받은 태그 이름으로 생성 시도
 *
 * POST    /tags
 * PARAMS  (REQUIRED) name 생성할 태그 이름
 *         (REQUIRED) desc 생성할 태그 설명
 * RETURN
 * - 성공: {success: 1, tags:[{_id, name, description}]}
 * - 실패: {success: 0, tags:[{_id, name, description}], error: {code, detail}}
 */
exports.tags_index_post = async function (req, res) {
  let tag_name = req.body["name"]
  let tag_desc = req.body["desc"]

  if (tag_name && tag_desc) {
    const record = await Tags.findOne({name: tag_name}, {_id:1, name:1, description:1})

    if (record == null) { // 기존 레코드가 없는 경우
      const newTag = new Tags({
              name: tag_name,
              description: tag_desc
            })
      await newTag
        .save()
        .then(result => {
          res.send({success: 1, tags: result})
        })
        .catch(error => {
          res.send({success: 0, tags:[], error: {code: 706, detail:error}})
        })
    } else { // 기존 레코드가 있는 경우
      res.send({success: 0, tags:record, error: {code: 702, detail:ErrorCode(702)}})
    }
  } else { // Required PARAMS 에러
    res.send({success: 0, tags: [], error: {code: 701, detail:ErrorCode(701)}})
  }
}

/**
 * tags_index_delete()
 *
 * DELETE   /tags
 * PARAMS   _id, name 중 하나를 반드시 Arguments로 전달해야 한다.
 * RETURN  성공 또는 실패 시 사용자가 삭제하고자 한 태그의 정보를 함께 반환한다.
 * - 성공: {success: 1, target:[{_id, name, description}]}
 * - 실패: {success: 0, target:[{_id, name, description}], error: {code, detail}}
 */
exports.tags_index_delete = async function (req, res) {
  let tag_name = req.body["name"]
  let tag_id = req.body["_id"]

  if (!tag_name && !tag_id){
    res.send({success: 0, tags:[], error: {code: 701, detail: ErrorCode(701)}})
  } else {
    const info = await Tags.findOne({$or: [{name: tag_name}, {_id: tag_id}]},
                                    {_id:1, name:1, description:1})

    if (info) {
      // 제거하고자 하는 레코드가 존재하는 경우
      await Tags.deleteOne({$or: [{name: tag_name}, {_id: tag_id}]},
                           function (err) {
        if (err) {
          res.send({success: 0, target: info,
                    error: {code: 704, detail: err}})
        } else {
          res.send({success: 1, target: info})
        }
      })
    } else {
      // 제거하고자 하는 레코드가 존재하지 않는 경우
      res.send({success: 0, error: {code: 704, detail: ErrorCode(704)}})
    }
  }
}

/**
 * tags_index_put(): 업데이트할 태그의 _id값을 이용하여 update
 *
 * PUT     /tags
 * PARAMS  {_id, name, updateTo: {name, description}}
 * RETURN  성공 또는 실패 시 업데이트 결과값을 함께 반환한다.
 * - 성공: {success: 1, target:[{_id, name, description}]}
 * - 실패: {success: 0, target:[{_id, name, description}], error: {code, detail}}
 */
exports.tags_index_put = async function (req, res) {
  let tag_name = req.body["name"]
  let tag_id = req.body["_id"]
  let updateTo = req.body["updateTo"]

  if (!tag_name && !tag_id && !updateTo){
    res.send({success: 0, tags:[], error: ErrorCode(701)})
  } else {
    await Tags.updateOne({$or: [{_id: tag_id}, {name: tag_name}]},
                {$set: updateTo},
                function (err, updated) {
                  if (err) {
                    res.send({success: 0, target:[updated],
                              error: {
                                code: ErrorCode(705),
                                detail: err
                              }})
                  } else {
                    res.send({success: 1, target: [updated]})
                  }
                })
  }
}
