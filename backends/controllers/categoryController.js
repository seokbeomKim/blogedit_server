const Category = require('../schemas/category.js')
const {getMessageWithCode: ErrorCode} = require('./errors.js')

/**
 * category_index_get()
 * 현재 데이터베이스에 저장되어 있는 카테고리 리스트를 반환한다.
 *
 * GET     /categories
 * PARAMS  (OPTIONAL) name: 카테고리 이름 (태그와 마찬가지로 명시하지 않을 경우 전체 반환)
 * RETURN  categories: [{_id, name, description, blogAccount}]
 */
exports.category_index_get = function (req, res) {
  const req_name = req.query
  

  if (req_name) {
    Category
      .find(req.query, {_id:1, name:1, description:1, blogAccount:1},
            function (err, data) {
              res.send({categories: data})
            })
  } else {
    Category
      .find({}, {_id:1, name:1, description:1, blogAccount:1},
            function (err, data) {
              res.send({categories: data})
            })
  }
}

/**
 * category_index_post()
 * 새 카테고리 등록
 *
 * POST    /categories
 * PARAMS  (REQUIRED) name            카테고리 이름
 *         (REQUIRED) desc            카테고리 설명
 *         (REQUIRED) account_idx     DB 내의 연관된 블로그 계정 인덱스
 * RETURNS
 * - 성공: {success: 1, categories: [{_id, name, description, blogAccount}]}
 * - 실패: {success: 0, categories: [], error: {code, detail}}
 */
exports.category_index_post = async function (req, res) {
  const name = req.body["name"]
  const desc = req.body["desc"]
  const account_idx = req.body["account_idx"]

  if (name && desc && account_idx) {
    const record = new Category({
            name: name,
            description: desc,
            blogAccount: account_idx
          })
    await record
      .save()
      .then(result => {
        res.send({success: 1, categories: result})
      })
      .catch(error => {
        res.send({success: 0, categories: [], error: {code: 706, detail: error}})
      })
  } else {
    res.send({error: {code: 701, detail: ErrorCode(701)}})
  }
}

/**
 * category_index_delete()
 * 카테고리 삭제
 *
 * DELETE    /categories
 * PARAMS    (REQUIRED) id: 삭제할 카테고리 _id
 * RETURNS
 * - 성공: {success: 1, categories: [{_id, name, description, blogAccount}]}
 * - 실패: {success: 0, categories: [], error: {code, detail}}
 */
exports.category_index_delete = async function (req, res) {
  const req_id = req.body["id"]

  if (!req_id) {
    res.send({success: 0, removed: [], error: {code: 701, detail: ErrorCode(701)}})
  } else {
    const info = await Category.findOne({_id: req_id})
    if (info) {
      Category.deleteOne({_id: req_id}, function (err) {
        if (err) {
          res.send({success: 0, removed: info, error: {code: 704, detail: err}})
        } else {
          res.send({success: 1, removed: info})
        }
      })
    } else {
      res.send({success: 0, error: {code:704, detail:ErrorCode(704)}})
    }
  }
}

/**
 * category_index_put()
 * 카테고리 업데이트
 *
 * PUT     /categories
 * PARAMS  (REQUIRED) id: 업데이트 할 카테고리 _id값
 *         (REQUIRED) updateTo {name, description}: 업데이트할 내용
 *                    (blogAccount를 수정하는 것은 불가능)
 */
exports.category_index_put = async function (req, res) {
  const req_id = req.body["id"]
  const updateTo = req.body["updateTo"]

  if (!req_id) {
    res.send({success: 0, error: {code: 701, detail: ErrorCode(701)}})
  } else {
    await Category
      .update({_id: req_id},
              {$set: updateTo},
              function (err, updated) {
                if (err) {
                  res.send({success: 0, updated: [updated],
                            error: {
                              code: 705,
                              detail: err
                            }})
                } else {
                  res.send({success: 1, updated: [updated]})
                }
              })
  }
}
