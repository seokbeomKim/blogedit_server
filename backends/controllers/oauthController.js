const {
  getMessageWithCode
} = require('./errors')
const tasker = require('../tasker')
const apis = require('../apis')

exports.oauth_get = function (req, res) {
  const target = tasker.dequeue('oauth') // oauth에 관련된 task만을 얻어옴  
  // response 처리해야 하는 플랫폼에 정의되어 있는 'access_code' 핸들러를 호출한다.
  const rvalue = apis[target.platform]['access_code_get'](req.query.code, target.auth)
  res.send('oauth_get' + rvalue)
}

exports.oauth_post = function (req, res) {
  console.log('oauth_post')
  res.send('oauth_post is called')
}
