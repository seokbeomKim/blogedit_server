/**
 * test.js
 *
 * 스키마 및 쿼리 테스트 위한 테스팅 스크립트
 */

var __DEBUG__ = true

if (__DEBUG__) {
// Require testing packages
// Use BDD interface for mocha
require('mocha')
require('mocha-steps')
var should = require('chai').should()
const mlog = require('mocha-logger')
const mongoose = require('mongoose')
mongoose.set('bufferCommands', true)

// Require schemas
const Tags          = require('./tag.js')
const BlogEngine    = require('./blogEngine.js')
const Category      = require('./category.js')
const CategoryGroup = require('./categoryGroup.js')
const BlogAccount   = require('./blogAccount.js')
const Post          = require('./post.js')

before(function (done) {
  // mongoose connection event callback functions
  let db = mongoose.connection

  db.on('error', (error) => {
    mlog.error('Failed to connect to mongodb: ', error)
    throw Error('Failed to establish db connection: ' + error)
  })
  db.on('connected', () => {
    mlog.log("The connection has been established.")
  })
  db.on('disconnected', () => {
    mlog.log("The connection has been disconnected successfully.")
  })
  db.once('open', function () {
    // 테스팅 시작하기 전 데이터베이스 초기화
    mongoose.connection.dropDatabase()
  })

  // try to connect
  mongoose.connect('mongodb://localhost:27017/blogedit',
                   {dbName: 'blogedit', useNewUrlParser: true},
                   done)
})


describe('Schema Design Verification Test', function () {
  context('Insert pseudo records for testing', function () {
    // Tags 레코드 추가 및 중복 검사
    context('Tags collection', function () {
      step('Insert TAGS records without errors', async () => {
        const records = new Tags({
                name: "blogedit",
                description: "This is a blogedit tag."
              })

        await records
          .save()
          .then(result => {
            mlog.log("Insertion completed successfully.")
          })
          .catch ((error) => {
            mlog.error("Insertion error" + error)
          })
      })
      step('Check duplicated insertion', async () => {
        const records = new Tags({
                name: "blogedit",
                description: "This is duplicated records. Needs to be fixed."
              })

        await records
          .save()
          .then(result => {
            mlog.error("Duplication check failed.")
          })
          .catch((error) => {
            mlog.success("Duplication check succeed.")
          })
      })
      step('Insert remained records', async function () {
        for (let i = 0; i < 10; ++i) {
          let records = new Tags({
                name: "tag" + i,
                description: "This is a tag generated from testcase. And the description could be duplicated"
              })

          await records
            .save()
            .then( () => {
            })
            .catch( (error) => {
            })
        }
      })
    })
    // Blog Engine 레코드 추가 및 중복 검사
    context('Blog engines collection: ', function () {
      step('Insert Blog engines ...', function () {
        // Define pseudo records
        const records = [
                {
                  name: "wordpress",
                  url: "https://wordpress.org"
                }, {
                  name: "tistory",
                  url: "https://tistory.com"
                }, {
                  name: "naver",
                  url: "https://blog.naver.com"
                }, {
                  name: "blogspot",
                  url: "https://blogspot.com"
                }
              ]
        BlogEngine
          .insertMany(records)
          .then ((docs) => {
            mlog.log("Insertion finished successfully.")
          })
          .catch ((error) => {
            mlog.error(error)
          })
      })
      step('Check duplicated insertions...', function () {
        const record = new BlogEngine({
                name: "wordpress",
                url: "This records should not be allowed."
              })
        record.save((error) => {
          should.exist(error)
        })
      })
    })
    // Blog Account 레코드 추가 및 중복 검사
    context('Blog account collection: ', function () {
      let records = []
      // 블로그 엔진에 해당하는 계정을 각각 생성한다.
      step('Insert blog accounts ...', function () {
        BlogEngine.find({}, async function (err, data) {
          should.not.exist(err)
          for (let d in data) {
            let record = new BlogAccount({
                  account: "sukbeom.kim@gmail.com",
                  blogEngine: data[d]._id,
                  auth: {
                    auth_url: "test_auth_url",
                    secret_key: "secret_string_key",
                    client_id: "test_client_id"
                  }
                })

            await record.save( (error) => {
              should.not.exist(error)
            })
          }
        })
      })
      step('Check duplicated insertions...', function () {
        // 이미 sukbeom.kim@gmail.com이라는 이름으로 여러 개
        // 생성되었으므로 해당 아이디로 테스트 진행
        BlogEngine.find({}, function (err, data) {
          const record = new BlogAccount({
                  account: "sukbeom.kim@gmail.com",
                  blogEngine: data[0]._id
                })
          record.save ( (err) => {
            should.exist(err)
          })
        })
      })
    })
    // Category 레코드 추가 및 스키마 정의 검사
    context('Category collection: ', function () {
      step('Insert categories ...', function () {
        const categories = [
                "Programming",
                "Programming/Web",
                "Linux",
                "Windows",
                "Mac OS",
                "Blog",
                "News",
                "Smart IoT",
                "Chaoxifer"
              ]
        BlogAccount.find({}, function (error, data) {

          if (error) {
            mlog.error("Unable to find blog accounts")
          }

          for (let d in data) {
            for (let category in categories) {
              const record = new Category({
                      name: categories[category],
                      description: 'This is a description for acategory',
                      blogAccount: data[d]._id
                    })
              record.save( (err) => {
                should.not.exist(err)
              })
            }
          }
        })
      })
      step('Check duplicated insertion...', function () {
        BlogAccount.findOne({}, {_id: 1}, function (error, data) {
          if (error) {
            mlog.error("Unable to find a record in blog accounts.")
            should.not.exist(error)
          }
          const record = new Category({
                  name: "Chaoxifer",
                  description: "Duplication test. This should not be inserted",
                  blogAccount: data._id
                })
          record.save( (err) => {
            should.exist(err)
          })
        })
      })
    })
    // Category Group 레코드 추가 및 스키마정의 검사
    context('Category group collection', function () {
      step('Insert category groups...', function () {
        BlogAccount.aggregate([{$sample: {size: 3}}, {$project: {_id: 1}}],
                              function (error, data) {
          if (error) {
            mlog.error("Unble to find a record in blog accounts.")
            should.not.exist(error)
          }

          for (let i = 0; i < 5; ++i) {
            let categories = []

            for (category in data) {
              categories.push(data[category])
            }

            const record = new CategoryGroup({
                    name: "Group" + i,
                    description: "This is a category group for testing",
                    categories: categories
                  })
            record.save((err) => {
              should.not.exist(err)
            })
          }
        })
      })
    })

    context('Post collection', function () {
      step('Insert a post...', async function () {
        let tags = []
        let link = []
        let category = []
        let category_group = []

        // 태그
        await Tags.aggregate([{$sample: {size: 3}}, {$project: {_id: 1}}],
                       function (err, data) {
                         if (err) {
                           mlog.error("Unable to find a record in tags.")
                           should.not.exist(err)
                         }
                         for (idx in data) {
                           tags.push(data[idx]._id)
                         }
                       })

        // 링크
        await BlogEngine.findOne({}, {_id:1}, function (err, data) {
          if (err) {
            mlog.err("Unable to find a record in engines.")
            should.not.exist(err)
          }
          link.push({
            url: "This is test url",
            engine: data._id
          })
        })

        // 카테고리
        await Category.aggregate([{$sample: {size:2 }}, {$project: {_id: 1}}],
                           function (err, data) {
                             if (err) {
                               mlog.error("Unable to find a record in category.")
                               should.not.exist(err)
                             }
                             for (idx in data) {
                               category.push(data[idx]._id)
                             }
                           })

        // 카테고리 그룹
        const record = new Post({
                title: "Hello world! for Writer!",
                content: "This editor should be used by professional writer",
                tag: tags,
                link: link,
                category: category,
                category_group: category_group
              })
        await record.save((err) => {
          should.not.exist(err)
        })
      })
    })
  })
})

after( function (done) {
  mongoose.connection.close(done)
})
}
