/**
 * categoryGroup.js
 *
 * 카테고리 그룹 스키마 정의: 모든 카테고리는 하나로 그룹화될 수
 * 있으며 사용자가 포스팅 시 사용자 설정한 카테고리로 한번에 포스팅할
 * 수 있도록 정의한다.
 */

const mongoose = require('mongoose')

const { Schema } = mongoose
const { Types: { ObjectId }} = Schema
const categoryGroupSchema = new Schema({
        name: {
          type: String,
          required: true,
          unique: true
        },
        description: {
          type: String,
          required: true
        },
        categories: [
          {
            type: ObjectId,
            ref: 'Category'
          }
        ]
      })

module.exports = mongoose.model('CategoryGroup', categoryGroupSchema)
