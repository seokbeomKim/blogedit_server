/**
 * blogAccount.js
 *
 * Blog engine 연동 계정
 */

const mongoose = require('mongoose')

const {
  Schema
} = mongoose
const {
  Types: {
    ObjectId
  }
} = Schema

// 'account': this.account,
// 'blogEngine': platformId,
// 'auth': {
// 'username': this.username,
// 'password': this.password,
// 'token': this.token,
// 'url': this.blogurl,
// 'client_id': this.clientid,
// 'secret_key': this.secretkey,
// 'callback_uri': this.callback_uri,
// 'access_token': this.access_token
// },
// 'blogUrl': this.blogurl,
// 'blogName': this.blogName
const blogAccountSchema = new Schema({
  account: {
    type: String,
    required: true,
  },
  blogEngine: {
    type: ObjectId,
    required: true,
    ref: 'BlogEngine'
  },
  blogName: String,
  blogUrl: String,
  blogNickname: String,
  auth: Object,
  comment: String
})

// Compound key 설정
blogAccountSchema.index({
  account: 1,
  blogEngine: 1,
  blogName: 1
}, {
  unique: true
})
module.exports = mongoose.model('BlogAccount', blogAccountSchema)
