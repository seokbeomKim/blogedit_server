/**
 * blogEngine.js
 *
 * 에디터에서 사용할 블로그 엔진 스키마 정의
 */

const mongoose = require('mongoose')
mongoose.set('useCreateIndex', true)

const { Schema } = mongoose
const engineSchema = new Schema({
        name: {
          type: String,
          required: true,
          unique: true
        },
        url: {
          type: String,            // 블로그 엔진 참고 URL
          required: true
        }
      })


module.exports = mongoose.model('BlogEngine', engineSchema)
