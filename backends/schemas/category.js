/**
 * category.js
 *
 * 포스팅 카테고리 정의 스키마
 */

const mongoose = require('mongoose')

const { Schema } = mongoose
const { Types: { ObjectId }} = Schema
const categorySchema = new Schema({
        name: {
          type: String,
          required: true
        },
        description: {
          type: String,
          required: true
        },
        blogAccount: {
          type: ObjectId,
          required: true,
          ref: 'BlogAccount'
        }
      })

categorySchema.index({name: 1, blogAccount:1}, {unique: true})
module.exports = mongoose.model('Category', categorySchema)
