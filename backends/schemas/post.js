/**
 * post.js
 *
 * blogedit에서 사용할 포스팅에 대한 스키마 정의
 */

const mongoose = require('mongoose')

const { Schema } = mongoose
const { Types: { ObjectId }} = Schema
const postSchema = new Schema({
        title: {
          type: String,
          required: true
        },
        content: {
          type: String,
          requred: true
        },
        tag: [{
          type: ObjectId,
          ref: 'Tag'
        }],
        link: [{
          url: String,
          engine: {
            type: ObjectId,
            ref: 'BlogEngine'
          }
        }],
        category: [{
          type: ObjectId,
          ref: 'Category'
        }],
        category_group: [{
          type: ObjectId,
          ref: 'CategoryGroup'
        }]
      })

module.exports = mongoose.model('Post', postSchema)
