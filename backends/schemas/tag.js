/**
 * tag.js
 *
 * 블로그 엔진에 관계없이 태그 설정할 수 있으며 태그는 그룹화하지
 * 않는다.
 */

const mongoose = require('mongoose')
mongoose.set('useCreateIndex', true);

const { Schema } = mongoose
const tagSchema = new Schema({
        name: {
          type: String,
          required: true,
          unique: true
        },
        description: {
          type: String,
          required: true
        }
      }, {bufferCommands: false})

module.exports = mongoose.model('Tag', tagSchema)
