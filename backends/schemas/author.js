/**
 * author.js
 *
 * 사용자가 필명을 설정할 수 있도록 설정한다. 단, 포스팅 시 사용자
 * 계정명을 기본으로 하는 곳은 author 커스터마이징이 불가능하므로
 * 스키마는 남겨놓되 현 버전에서는 사용하지 않는다.
 */

const mongoose = require('mongoose')
const { Schema } = mongoose
const authorSchema = new Schema({
        author: String,
      })

module.exports = mongoose.model('Author', authorSchema)
