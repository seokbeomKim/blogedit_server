/**
 * mongoose 사용하여 blogedit에서의 db 스키마 정의
 */
const mongoose = require('mongoose')

module.exports = () => {
  let db = mongoose.connection

  db.on('error', (error) => {
    console.error('Failed to connect to mongodb: ', error)
    throw Error('Failed to establish db connection: ' + error)
  })
  db.on('connected', () => {
    console.info("DB connection has been established.")
  })
  db.on('disconnected', () => {
    console.info("DB connection has been disconnected successfully.")
  })
  // db.once('open', function () {
  //   // 테스팅 시작하기 전 데이터베이스 초기화
  //   mongoose.connection.dropDatabase()
  // })

  // try to connect
  mongoose.connect('mongodb://localhost:27017/blogedit',
                   {dbName: 'blogedit', useNewUrlParser: true})

  // 데이터베이스 스키마 정의 파일 연결
  require('./blogEngine.js')
  require('./tag.js')
  require('./category.js')
  require('./categoryGroup.js')
  require('./blogAccount.js')
  require('./post.js')
}
