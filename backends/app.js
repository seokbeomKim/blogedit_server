var createError = require('http-errors');
var cookieParser = require('cookie-parser');
var express = require('express');
var session = require('express-session')
var path = require('path');
var logger = require('morgan');
var faviconIgnore = require('blogedit/libs/ignoreFavicon')

var webSocket = require('blogedit/libs/sockets')
var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')
var tagsRouter = require('./routes/tags')
var usersRouter = require('./routes/users')
var postRouter = require('./routes/posts')
var platformRouter = require('./routes/platforms')
var categoryRouter = require('./routes/categories')
var categoryGroupRouter = require('./routes/categoryGroup.js')
var oauthRouter = require('./routes/oauth')

require('dotenv').config()

var dbconnect = require('./schemas');
var app = express();

// Connect to mongodb database
dbconnect();

const sessionMiddleware = session({
  resave: false,
  saveUninitialized: false,
  secret: process.env.COOKIE_SECRET,
  cookie: {
    httpOnly: true,
    secure: false
  }
})

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('port', process.env.PORT || 8005)

app.use(logger('dev'));
app.use(faviconIgnore)
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(sessionMiddleware)

// Define routers for RESTful API
app.use('/', indexRouter)
app.use('/tags', tagsRouter)
app.use('/categories', categoryRouter)
app.use('/platforms', platformRouter)
app.use('/users', usersRouter)
app.use('/posts', postRouter)
app.use('/category_groups', categoryGroupRouter)
app.use('/oauth', oauthRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

const server = app.listen(app.get('port'), () => {
  console.log('Listening port: ', app.get('port'))
})

webSocket.initialize(server, app, sessionMiddleware)
